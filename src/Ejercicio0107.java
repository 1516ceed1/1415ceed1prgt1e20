/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-oct-2013
 */
class Ejercicio0107 {

  static public void main(String[] args) {
    int num = 5;
    num += num - 1 * 4 + 1; // Expresion 1
    System.out.println(num);
    num = 4;
    num %= 7 * num % 3 * 7 >> 1; // Expresion 2
    System.out.println(num);
  }
}
