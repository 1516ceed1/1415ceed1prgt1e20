/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-oct-2013
 */
class Ejercicio0112 {

  public static void main(String[] args) {
    boolean adivina = ((97 == 'a') && true);
    System.out.println(adivina);
    int a = 1;
    int b = a >>> 2;
    System.out.println(b);
    a = 7 | 4;
    System.out.println(a);
    b = 3 | 4;
    System.out.println(b);
    a = 7 & 4;
    System.out.println(a);
    b = 3 & 4;

  }

}
