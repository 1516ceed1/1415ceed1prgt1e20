/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-oct-2013
 */
class Ejercicio0113 {

  public static void main(String[] args) {
    int edad = 29, nivel_de_estudios = 4, ingresos = 30000;
    boolean jasp
            = ((edad <= 28 && nivel_de_estudios > 3) || (edad < 30 && ingresos > 28000))
                    ? true : false;
    System.out.println(jasp);
  }
}

/* EJECUCION:
true
*/
