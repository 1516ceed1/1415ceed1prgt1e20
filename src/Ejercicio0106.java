/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Ejercicio0106 {

  public static void main(String[] args) {
    int numero = 2,
            cuad = numero * numero;
    System.out.println("EL CUADRADO DE " + numero + " ES: " + cuad);
  }
}
