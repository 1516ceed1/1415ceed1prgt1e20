/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-oct-2013
 */
class Ejercicio0108 {

  public static void main(String[] args) {
    int radio = 3;
    final double PI = 3.141592;
    System.out.println(2 * PI * radio);
  }
}

/* EJECUCION:
18.849552000000003
*/
