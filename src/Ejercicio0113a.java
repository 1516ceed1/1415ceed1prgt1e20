/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-oct-2013
 */
class Ejercicio0113a {

  public static void main(String[] args) {
    int edad = 29;
    int nivelEstudios = 4;
    int ingresos = 29000;
    boolean jasp;
    jasp = false;
    if (((edad <= 28) && (nivelEstudios > 3))
            || ((edad < 30) && (ingresos > 28000))) {
      jasp = true;
      System.out.println(jasp);
    }
  }
}
/* EJECUCION:
true
*/
