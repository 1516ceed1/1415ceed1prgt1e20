/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Ejercicio0102a {

  public static void main(String args[]) {
    boolean a;
    boolean b;

    System.out.println("A      B      NOT A   A AND B   A OR B   A XOR B");
    a = false;
    b = false;
    String s = String.format("%5b  %5b  %5b   %5b     %5b    %5b", a, b, !a, a && b, a || b, a ^ b);
    System.out.println(s);
    // %b indica que es booleano y el 5 indica que debe ocupar cinco espacios.
    a = false;
    b = true;
    String t = String.format("%5b  %5b  %5b   %5b     %5b    %5b", a, b, !a, a && b, a || b, a ^ b);
    System.out.println(t);

    a = true;
    b = false;
    String u = String.format("%5b  %5b  %5b   %5b     %5b    %5b", a, b, !a, a && b, a || b, a ^ b);
    System.out.println(u);

    a = true;
    b = true;
    String v = String.format("%5b  %5b  %5b   %5b     %5b    %5b", a, b, !a, a && b, a || b, a ^ b);
    System.out.println(v);
  }
}
